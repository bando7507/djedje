export const fadeIn = (direction: string, delay: number) => {
    return{
        hidden: {
            y: direction === 'up' ? 80 : direction === 'down' ? -80 : 0,
            opacity: 0,
            x: direction === 'left' ? 80 : direction === 'right' ? -80 : 0,
        },
        show: {
            y: 0,
            x: 0,
            opacity: 1,
            transition: {
                type: 'tween',
                duration: .3,
                delay: delay,
                ease: [0.25, 0.25, 0.25, 0.75]
            }
        }
    }
}


export const fadeIn2 = (direction: string, delay: number) => {
    return{
        hidden: {
            opacity: 0,
            scale: 0,
        },
        show: {
            y: 0,
            x: 0,
            opacity: 1,
            scale: 1,
            transition: {
                // type: 'tween',
                duration: .6,
                delay: delay,
                ease: [0.25, 0.25, 0.25, 0.75]
            }
        },
        exit: {
            opacity: 1,
            scale: 1
        }
    }
}
// +f6qjr^JPyU*P#"