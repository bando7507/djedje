export const offreSlide: {id: number, type: string, offre: string, temps: string}[] = [
    {
        id: 1,
        type: 'CDI',
        offre: 'Technicien(ne) Informatique Système et Réseau',
        temps: 'Il y a 4 heures'
    },
    {
        id: 2,
        type: 'CDI',
        offre: 'Technicien support de proximité H/F',
        temps: 'Il y a 2 heures'
    },
    {
        id: 3,
        type: 'Freelance ',
        offre: 'Ingénieur Systèmes & Réseaux',
        temps: 'Il y a 7 heures'
    },

]


export const cardOffre: {id: number, title: string, name: string, type: string, published: string, wallet: string, delay?: string, description: string,
    description2?: string, localisation: string, time?: string}[] = [
    {
        id: 1,
        type: 'CDD',
        title: 'Ingénieur Développeur C#',
        name: 'R&S TELECOM 2',
        published: '13/11/2023',
        wallet: '65 K',
        description: 'Analyser les développements existants et assurer leur maintenance et évolution.Examiner les spécifications fonctionnelles.Développer, maintenir et faire évoluer des applications métiers.Rédiger et exécuter des tests unitaires et des tests d’intégration.Identifier et résoudre les dysfonctionnements, en assurant la correction des anomalies.Participer au déploiement des changements dans les environnements de recette, pré production et production.Rédiger des documentations techniques.Développer des solutions répondant aux besoins métiers, tout en respectant les objectifs de qualité, de délai et de budget, et en optimisant la maintenance.Assurer le support utilisateurs en fonction des impacts opérationnels.Effectuer une veille technologique sur les composants techniques.Développer en tenant compte de la sécurité du système d’information.',
        description2: 'R&S Telecom est une entreprise de services numériques et une équipe d’ingénieurs Telecom qui vous accompagnent dans vos projets en vous fournissant l’expertise et les conseils. Depuis 2014, notre expertise dans le conseil en intégration de nouvelles technologies, nous permet de proposer une approche globale de la gestion de projet alliant réflexion, accompagnement et assistance technique.',
        localisation: 'Viry-Châtillon, Île-de-France',
        time: '2 ans'
    },
    {
        id: 2,
        title: 'Ingénieur de production H/F',
        type: 'CDI',
        name: 'ECONOCOM INFOGERANCE ET SYSTEME',
        published: '01/03/2023 ',
        wallet: '65 K',
        description: `La Direction des Services Econocom en Occitanie recherche un Ingénieur de Production H/F pour l'accompagner dans le développement de son activité.
        Vous rejoignez notre entité Econocom Services Workplace Infra Innovation sur la région Occitanie.
        Au sein d'un acteur majeur du domaine des banques et assurances, vous intégrez une équipe, vous assurerez les missions suivantes :
        • Mise en œuvre des solutions techniques
        • Réalisation des diagnostics
        • Proposition et mise en œuvre des corrections et des solutions de \"back-up\"
        • Proposition des solutions pour améliorer les performances de son domaine d'activité.
        • Définir les règles de bonne gestion des systèmes d'exploitation
        • Mise à niveau de l'assemblage, la cohérence et l'homologation des différents logiciels (progiciels, base de données, développements spécifiques)
        • Prise en charge de l'intégration de nouvelles applications ou de progiciels (transactionnel et/ou batch)
        • Effectue la réception, la validation et le packaging des composants
        • Installe et met à disposition des composants.
        • Réalise la recette technique et l'homologation des composants / applications.
        • Participe à l'amélioration des processus et des outils d'industrialisation et de déploiement
        • Travaille en relation étroite et permanente avec les équipes Etudes, Infrastructures et Sécurité.
        
        Spécificités des tests techniques :
        • Prise de connaissance du changement et de son contexte technique
        • Prépare les infrastructures de tests
        • Valide les documents et les procédures à destination de la production
        • Réalise les différents tests techniques
        • Analyse les résultats
        • Contribue au diagnostic en cas de dysfonctionnement
        • Rédige les rapports détaillés qui aideront à la décision pour la mise en production
        • Constitue ou met à jour le patrimoine de tests réutilisable
        • Effectue le reporting vers le Responsable Intégration
        • Contribue à l'amélioration continue
        • Industrialise les tests techniques récurrents (automatisation)
        
        
        Des missions complémentaires peuvent vous être confiées.
        
        Référence de l'offre : 79wms252eh`,
        description2: `Econocom est un groupe européen, qui met en œuvre la transformation digitale des entreprises et des organisations publiques en plaçant les utilisateurs finaux au coeur de chaque projet. Première Entreprise Générale du Digital en Europe, le groupe Econocom conçoit, finance et facilite la transformation digitale des grandes entreprises et des organisations publiques.`,
        localisation: 'Toulouse',
        time: 'null'
    },
    {
        id: 3,
        title: 'Ingénieur Développeur C# .NET',
        type: 'Freelance',
        name: 'R&S TELECOM',
        published: '02/02/2023',
        wallet: '65 K',
        description: 'Analyser les développements existants et assurer leur maintenance et évolution.Examiner les spécifications fonctionnelles.Développer, maintenir et faire évoluer des applications métiers.Rédiger et exécuter des tests unitaires et des tests d’intégration.Identifier et résoudre les dysfonctionnements, en assurant la correction des anomalies.Participer au déploiement des changements dans les environnements de recette, pré production et production.Rédiger des documentations techniques.Développer des solutions répondant aux besoins métiers, tout en respectant les objectifs de qualité, de délai et de budget, et en optimisant la maintenance.Assurer le support utilisateurs en fonction des impacts opérationnels.Effectuer une veille technologique sur les composants techniques.Développer en tenant compte de la sécurité du système d’information.',
        description2: 'R&S Telecom est une entreprise de services numériques et une équipe d’ingénieurs Telecom qui vous accompagnent dans vos projets en vous fournissant l’expertise et les conseils. Depuis 2014, notre expertise dans le conseil en intégration de nouvelles technologies, nous permet de proposer une approche globale de la gestion de projet alliant réflexion, accompagnement et assistance technique.',
        localisation: 'Viry-Châtillon, Île-de-France',
        time: '1 ans'
    },
    {
        id: 4,
        type: 'Freelance',
        title: 'Architecte Poste de Travail Intune Paris',
        name: 'WINSIDE Technology',
        published: '30/09/2023',
        wallet: '65 K',
        description: `1 - Contexte :
        L’ingénieur Senior Workplace fera partie de l’équipe Windows Edition Product, aidant à l’intégration et au déploiement de l’appareil géré Intune, soutenant l’onboarding des entités du groupe à travers la configuration du profil et les activités d’intégration d’applications.2 - Description du service :
        - Gestion des applications de bout en bout (intégration-test-qualification-déploiement) dans Intune.
        - Activités de support L3 (incidents majeurs, problèmes techniques rencontrés par les utilisateurs)
        - Analyse des bogues/corrections
        - Développement de fonctionnalités si nécessaire
        - Contribution à la gestion du cycle de vie du produit sur l’entité locale
        
        3 - Produits livrables :
        - Paquets d’applications/ configurés par Intune/WIP/VPN
        - Mise à jour/résolution/fermeture des tickets d’incidents SILVA
        - Rapports (éléments de sécurité, nombre d’appareils, rapports sur les applications/outils) via Intune/Log Analytics
        - Documentation de tout ce qui sera implémenté/configuré
        
        4 - Expertises :
        
        Doit avoir des expertises :
        - Anglais courant
        - Rédaction d’expertises à des fins de documentation
        - Approche MDM via Autopilot/Intune
        - Expertise Windows Devices
        - Windows Information Protection (WIP)
        
        Expertises souhaitées
        - MS Endpoint DLP
        - McAfee Cloud Proxy
        - F5 PerApp VPN
        - Méthodologie Agile/Scrum`,

        description2: 'Winside est une Entreprise de Services du Numérique. Notre valeur ajoutée réside dans notre appréhension des nouveaux enjeux de la digitalisation, dans la recherche continue de nouvelles technologies et méthodes. Winside vous apporte des solutions adaptées à chacun de vos besoins et ambitions tout au long de vos projets.',
        localisation: 'Viry-Châtillon, Île-de-France',
        time: '2 ans'
    },
]