"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import {
  ArrowRight,
  Clock1,
  Computer,
  FileTextIcon,
  Files,
  HeartHandshake,
  Lock,
  LogOut,
  Mail,
  MailCheck,
  MapPin,
  MapPinIcon,
  Megaphone,
  MessageCircle,
  PauseCircle,
  Phone,
  PieChart,
  PlusCircle,
  ThumbsUp,
  Users2,
  Wallet2,
} from "lucide-react";

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";

import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

import { z } from "zod";
import { motion } from "framer-motion";
import { Button } from "@/components/ui/button";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import Hero from "./_components/Hero";
import Counter from "./_components/Counter";
import Footer from "./_components/Footer";
import { fadeIn, fadeIn2 } from "@/variants";
import { useForm } from "react-hook-form";

// Import Swiper styles
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";

import "../../style/style.css";

// import required modules

import { useMediaQuery } from "usehooks-ts";

import "leaflet/dist/leaflet.css";

export default function Home() {
  var isMobile = useMediaQuery("(max-width: 768px)");
  var isTabletsss = useMediaQuery("(max-width: 1024px)");

  const cardSchema = z.object({
    title: z.string(),
    description: z.string(),
    icon: z.any(),
    item1: z.string().optional(),
    item2: z.string().optional(),
    item3: z.string().optional(),
    item4: z.string().optional(),
    item5: z.string().optional(),
  });

  type cardSchema = z.infer<typeof cardSchema>;

  const cardData: cardSchema[] = [
    {
      title: "Centre de formation initial",
      description:
        "En un clic, votre annonce est diffusée sur des centaines de sites emploi.",
      icon: <Megaphone size={45} color="#d9a13b" />,
      item1: "Proffesseurs des CAFOP",
      item2: "Proffesseurs des LYCEES",
      item3: "Proffesseurs des COLLEGES",
      item4: "Proffesseurs agreges des classe preparatiores",
      item5: "...",
    },
    {
      title: "Centre de formation continue",
      description:
        "Gagnez du temps en centralisant les candidatures sur une plateforme unique.",
      icon: <LogOut size={45} color="#d9a13b" />,
      item1: "Institue d'anglais intensif",
      item2: "Universite de vacances",
      item3: "Cours de preparation aux concours",
    },
    {
      title: "Appui Pedagogique en ressources informatiques et documents",
      description:
        "Analysez la performance de vos canaux de diffusion et optimisez vos investissements.",
      icon: <PieChart size={45} color="#d9a13b" />,
      item1: "Audiovisuel et numerique pedagogique",
      item2: "Maintenmance informatique et reseaux",
      item3: "Biblitheque et reprographie",
    },
    {
      title: "Centre de recherche en education et productions",
      description:
        "Boostez votre marque employeur avec un site aux couleurs de votre entreprise.",
      icon: <Computer size={45} color="#d9a13b" />,
      item1: "Recherche scientifique",
      item2: "Revues et publiacations scientifiques",
      item3: "Materiels didactiques",
      item4: "Cooperation et relations exterieures",
    },
    {
      title: "Ecole doctorale",
      description:
        "Programmez et envoyez automatiquement vos réponses par email ou SMS à vos candidats.",
      icon: <Users2 size={45} color="#d9a13b" />,
      item1:
        "Formation doctorale en sciences de l'education et de la formation",
    },
    {
      title: "Etabliessement d'application JEAN PIAGET",
      description:
        "En un clic, votre annonce est diffusée sur des centaines de sites emploi.",
      icon: <MessageCircle size={45} color="#d9a13b" />,
    },
  ];

  const [ets, setEts] = useState("Toutes");
  const FormSchema = z.object({
    type: z.enum(["Toutes", "Freelance", "Worker"], {
      required_error: "You need to select a notification type.",
    }),
  });

  function onSubmit(data: z.infer<typeof FormSchema>) {
    console.log(data);
  }

  function onChawc(data: any) {
    console.log(data);
    setEts(data);
  }

  const [numSlide, setNumSlide] = useState<number>(3);

  useEffect(() => {
    // console.log(isMobile);
    if (isMobile) {
      setNumSlide(1);
    } else if (isTabletsss) {
      setNumSlide(2);
    } else {
      setNumSlide(3);
    }
  }, [isMobile]);

  return (
    <div className=" dark:text-white">
      <Hero />
      <main className="w-full overflow-hidden">
        <section className="m-auto w-full h-full px-5 mt-12 lg:max-w-5xl md:flex gap-7">
          <div>
            <Image
              src="/dg1.jpeg"
              alt=""
              width={500}
              height={500}
              className="w-full object-contain "
            />
          </div>

          {/* </motion.div> */}
          <div className="mt-8  md:m-0 ">
            <motion.h1
              variants={fadeIn("left", 0.3)}
              initial="hidden"
              whileInView={"show"}
              viewport={{ once: false, amount: 0.6 }}
              className="text-3xl text-green-600 font-ubuntu font-bold mb-4 lg:text-4xl"
            >
              Bamory Kamagaté
            </motion.h1>

            <motion.h2
              className="my-4 flex items-center gap-2
            "
            >
              <MailCheck />
              Ecrire au Directeur
            </motion.h2>

            <motion.p
              variants={fadeIn("left", 0.5)}
              initial="hidden"
              whileInView={"show"}
              viewport={{ once: false, amount: 0.6 }}
              className="font-poppins mb-8"
            >
              L&apos;ENS est dirigée par un Directeur Général nommé par décret
              en Conseil des Ministres, sur proposition conjointe des Ministres,
              membres du Conseil de gestion. Il a rang de Directeur Général
              d&apos; &apos;Administration Centrale. Il est choisi parmi les
              professeurs de rang magistral. Le Directeur Général est
              l&apos;ordonnateur principal de l&apos;ENS. Il est investi des
              pouvoirs nécessaires pour en assurer l&apos;administration et la
              direction.
            </motion.p>
          </div>
        </section>
        {/* <section className="bg-green-400">
          <motion.div
            variants={fadeIn("down", 0.3)}
            initial="hidden"
            whileInView={"show"}
            viewport={{ once: false, amount: 0.6 }}
            className="py-20 m-auto w-full h-full px-5 mt-12 lg:max-w-6xl md:flex gap-7"
          >
            <div className="w-full grid grid-cols-2 gap-4 place-items-center md:grid-cols-3 lg:grid-cols-4">
              <div className="flex flex-col gap-2">
                <div>
                  <FileTextIcon color="#d9a13b" size={40} />
                </div>
                <div>
                  <h1 className="font-ubuntu font-bold text-2xl md:text-3xl lg:text-4xl">
                    <Counter end={100000} duration={2} />
                  </h1>
                  <p className="text-sm font-poppins font-bold">
                    candidatures reçues
                  </p>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div>
                  <Files color="#d9a13b" size={40} />
                </div>
                <div>
                  <h1 className="font-ubuntu font-bold text-2xl  md:text-3xl lg:text-4xl">
                    <Counter end={30000} duration={2} />
                  </h1>
                  <p className="text-sm font-poppins font-bold">
                    offres d&apos;emploi publiées
                  </p>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div>
                  <ThumbsUp color="#d9a13b" size={40} />
                </div>
                <div>
                  <h1 className="font-ubuntu font-bold text-2xl  md:text-3xl lg:text-4xl">
                    <Counter end={80} duration={2} />
                  </h1>
                  <p className="text-sm font-poppins font-bold">
                    % d&apos;utilisateurs satisfaits
                  </p>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div>
                  <Users2 color="#d9a13b" size={40} />
                </div>
                <div>
                  <h1 className="font-ubuntu font-bold text-2xl  md:text-3xl lg:text-4xl">
                    <Counter end={20000} duration={2} />
                  </h1>
                  <p className="text-sm font-poppins font-bold">utilisateurs</p>
                </div>
              </div>
            </div>
          </motion.div>
        </section> */}

        <section className="m-auto w-full h-full px-5 mt-12 lg:max-w-5xl ml-auto mr-auto">
          <h1 className="text-center font-ubuntu font-bold text-2xl md:text-3xl lg:text-4xl mb-4">
            {/* Tous savoir sur l&apos;ENS */}
            ENS D&apos;ABIDJAN
          </h1>

          <p className="mb-7">
            L&apos;Ecole Normale Supérieure d&apos;Abidjan est un établissement
            d&apos;enseignement Supérieur. Elle est habilitée à réaliser des
            prestations de formation initiale et continue au bénéfice du
            personnel de l&apos;Education Nationale et des prestations de
            conseil et de formation au bénéfice de partenaires extérieures
            publics ou privés.
          </p>

          <div className="grid items-center place-content-center gap-6 md:grid-cols-2">
            <div>
              <h2 className=" font-ubuntu font-bold text-lg md:text-3xl lg:text-2xl mb-6">
                Notre mission
              </h2>

              <p className="mb-6">
                Créer en 1964, l&apos;ENS est un Etablissement public
                d&apos;Enseignement Supérieur. Elle est sous la tutelle du
                Ministère de l&apos;Enseignent Supérieur. Elle est chargée :
              </p>

              <ul
                className=" flex
               flex-col gap-3 list-disc text-[11px] "
              >
                <li>
                  de la formation et du perfectionnement pédagogique des
                  enseignants de l&apos;Enseignement Secondaire Général, des
                  CAFOP, des inspecteurs de l&apos;enseignement secondaire, du
                  primaire et du préscolaire
                </li>
                <li>
                  de la formation du personnel d&apos;encadrement administratif
                  et de la vie scolaire et extrascolaire
                </li>
                <li>de la recherche en éducation</li>
                <li>de la production et la diffusion du matériel didactique</li>
                <li>
                  Lde la réalisation de prestations de formation initiale et
                  continue au bénéfice du personnel de l&apos;Education
                  Nationale
                </li>
                <li>
                  des prestations de conseils et de formation au bénéfice des
                  partenaires extérieurs publics ou privés.
                </li>
              </ul>
            </div>

            <div className="place-content-center">
              <Image
                src="/ens1.jpeg"
                alt=""
                width={1000}
                height={500}
                className="w-full object-contain "
              />
            </div>
          </div>

          <div>
            <h1 className=" mt-6 font-ubuntu font-bold text-lg md:text-3xl lg:text-2xl mb-6">
              Le personnel
            </h1>
            <p className="mb-4">
              On distingue deux (2) types de personnels au sein de
              l&apos;Institution : le personnel enseignant d&apos;une part et le
              personnel technique et administratif d&apos;autre part.{" "}
            </p>

            <p className="mb-4">
              Le personnel enseignant : Il s&apos;agit du corps professoral
              chargé des enseignements, des encadrements et de la recherche. Il
              comprend des Professeurs titulaires, des Maîtres de conférences,
              des Maîtres Assistants, des Professeurs Certifiés et Licenciés,
              des Inspecteurs de l&apos;enseignement primaire et secondaire, des
              Professeurs de CAFOP, des Ingénieurs en Informatique...{" "}
            </p>

            <p className="mb-4">
              <span className="italic">
                Le personnel technique et administratif
              </span>{" "}
              : contribue aux fonctionnements des services administratifs et des
              laboratoires, à l&apos;animation et à l&apos;entretien de
              l&apos;établissement.
            </p>
          </div>
        </section>

        <section className="bg-green-400 py-20 m-auto w-full h-full px-20 mt-12 lg:max-w-6xl gap-7 shadow-custom">
          <motion.div
            variants={fadeIn("down", 0.3)}
            initial="hidden"
            whileInView={"show"}
            viewport={{ once: false, amount: 0.6 }}
            className="w-full mb-8 md:flex justify-between md:m-0"
          >
            <div>
              <h1 className="font-ubuntu font-bold text-2xl md:text-3xl lg:text-4xl mb-5">
                Direction Générale
              </h1>
              <p className="text-sm font-poppins mb-5 md:text-base">
                L&apos;ENS est dirigée par un Directeur Général nommé par décret
                en Conseil des Ministres, sur proposition conjointe des
                Ministres, membres du Conseil de gestion. Il a rang de Directeur
                Général d&apos;Administration Centrale. Il est choisi parmi les
                professeurs de rang magistral. Le Directeur Général est
                l&apos;ordonnateur principal de l&apos;ENS. Il est investi des
                pouvoirs nécessaires pour en assurer l&apos;administration et la
                direction.
              </p>

              <p className="mb-5 mt-5">
                La Direction Générale de l&apos;ENS comprend (06) Directions
                Centrales et deux Services rattachés :
              </p>

              <p className="text-xl mb-5">
                • Les Directions Centrales sont au nombre de six (06) :{" "}
              </p>

              <ul
                className=" flex
               flex-col gap-3 list-disc text-[11px] "
              >
                <li>La Direction de la Formation Initiale</li>
                <li>La Direction de la Formation Continue</li>
                <li>La Direction de la scolarité, des examens et concours</li>
                <li>La Direction de la Recherche et des Publications</li>
                <li>
                  La Direction des Affaires Administratives et Financières
                </li>
                <li>
                  La Direction des Appuis pédagogiques et des Ressources
                  Informatiques et Documentation.
                </li>
              </ul>

              <p className="text-xl mb-5 mt-5">
                • Les Services directement rattachés à la Direction Générale
                sont :
              </p>

              <ul
                className=" flex
               flex-col gap-3 list-disc text-[11px] "
              >
                <li>Le Service Communication</li>
                <li>L&apos;Etablissement d&apos;Application Jean Piaget.</li>
              </ul>
            </div>
          </motion.div>
        </section>

        <section className="pt-20">
          <h1 className="text-center text-2xl">Nos partenaire</h1>

          <div className="grid gap-3 grid-cols-1 md:grid-cols-2 lg:grid-cols-3 place-items-center">
            <div className="">
              <Image
                src="/injs.png"
                alt=""
                width={150}
                height={100}
                className=" object-contain "
              />
            </div>
            <div>
              <Image
                src="/inp.png"
                alt=""
                width={150}
                height={100}
                className=" object-contain "
              />
            </div>
            <div>
              <Image
                src="/vitib.png"
                alt=""
                width={150}
                height={100}
                className=" object-contain "
              />
            </div>
          </div>
        </section>

        <motion.section
          variants={fadeIn2("down", 0.3)}
          initial="hidden"
          whileInView={"show"}
          viewport={{ once: false, amount: 0.6 }}
          className=" py-10  m-auto w-full h-full px-20 mt-12 lg:max-w-6xl  gap-7"
        >
          <div className="w-full md:flex justify-between">
            <div className="mb-8 md:m-0">
              <h1 className="text-3xl  font-poppins font-bold mb-4 lg:text-4xl">
                ETUDES
              </h1>

              <h2 className=" font-ubuntu font-bold text-lg md:text-3xl lg:text-2xl mb-6">
                Départements
              </h2>
              <p className="text-sm font-poppins  md:text-base">
                Les enseignements sont assurés dans le cadre des départements et
                des sections.
              </p>
              <p className="text-sm font-poppins  md:text-base">
                L&apos;ENS compte 5 départements dont certains sont subdivisés
                en sections :
              </p>
            </div>
            {/* <Button variant="green" className="group/btn transition">
              Parlez à un expert
              <div className=" opacity-0 group-hover/btn:opacity-100 group-hover/btn:translate-x-3 transition">
                <ArrowRight />
              </div>
            </Button> */}
          </div>

          <div>
            <Accordion type="single" collapsible className="w-full">
              <AccordionItem value="item-1">
                <AccordionTrigger>
                  Département des Sciences de l&apos;Education :
                </AccordionTrigger>
                <AccordionContent>
                  composante transversale des formations pédagogiques, ce
                  département étend ses activités dans tous les autres
                  départements. II couvre toutes les formations dispensées aux
                  seins de l&apos;établissement. Ce département comprend les
                  filières de formation des inspecteurs de l&apos;enseignement
                  primaire, des Inspecteurs d&apos;Education, des Educateurs,
                  des professeurs de CAFOP et des Inspecteurs
                  d&apos;Orientation.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-2">
                <AccordionTrigger>Départements des Langues :</AccordionTrigger>
                <AccordionContent>
                  il comprend trois sections préparant aux diplômes de formation
                  des professeurs de l&apos;enseignement secondaire : section
                  d&apos;allemand, d&apos;anglais, et d&apos;espagnol.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-3">
                <AccordionTrigger>
                  Département des Lettres et des Arts :
                </AccordionTrigger>
                <AccordionContent>
                  il comprend les sections lettres modernes, arts, et
                  linguistique : on y prépare les différents diplômes
                  d&apos;accès aux fonctions de professeurs des lycées et
                  collèges.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-4">
                <AccordionTrigger>
                  Département d&apos;Histoire et Géographie :
                </AccordionTrigger>
                <AccordionContent>
                  il prépare aux différents diplômes d&apos;accès aux métiers de
                  professeurs des lycées et collèges en histoire et géographie.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-5">
                <AccordionTrigger>
                  Départements des Sciences et Technologies :{" "}
                </AccordionTrigger>
                <AccordionContent>
                  il comprend trois sections : la section sciences de la vie et
                  de la terre, la section sciences physiques, et la section
                  mathématiques. On y prépare les mêmes diplômes que dans les
                  autres départements.
                </AccordionContent>
              </AccordionItem>
            </Accordion>
          </div>
        </motion.section>

        <section className="border-b-2 border-t-2 mt-8 grid md:grid-cols-3">
          <div className="border-b-2 p-8 md:border-r-2 md:border-b-0">
            <Mail size={45} />
            <h2>Adresse e-mail</h2>
            <p className="text-sm font-poppins text-[#d9a13b] md:text-base font-bold">
              info@ens.ci
            </p>
          </div>
          <div className="border-b-2 p-8 md:border-r-2 md:border-b-0">
            <Phone size={45} />
            <h2>Contactez-nous</h2>
            <p className="text-sm font-poppins text-[#d9a13b] md:text-base font-bold">
              (+225) 22 44 42 32
            </p>
          </div>
          <div className="p-8">
            <MapPinIcon size={45} />

            <h2>Essayez maintenant</h2>
            <p className="text-sm font-poppins text-[#d9a13b] md:text-base font-bold">
              Abidjan Côte D&apos;Ivoire <br />
              08 BP 10 Abidjan 08
            </p>
          </div>
        </section>

        {/* <section className="m-auto w-full h-full px-5 mt-12 lg:max-w-5xl md:flex gap-7 flex-col">
          <h1 className="text-center font-ubuntu font-bold text-2xl md:text-3xl lg:text-4xl mb-4">
            Localisation
          </h1>
        </section>

        <MapContainer
          center={[5.3366657, -4.1067393]}
          zoom={13}
          scrollWheelZoom={false}
          style={{ height: "400px", width: "100%" }}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
          />

          <Marker position={[5.3366657, -4.1067393]}>
            <Popup>
              ENS <br /> Ecole Normale Supérieure d&apos;Abidjan.
            </Popup>
          </Marker>
        </MapContainer> */}
      </main>
      <Footer />
    </div>
  );
}
