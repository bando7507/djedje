"use client";
import { Button } from "@/components/ui/button";
import { ArrowRight } from "lucide-react";
import React from "react";
import { motion, easeInOut } from "framer-motion";
import { fadeIn } from "@/variants";
const Hero = () => {
  return (
    <section className="bg-pattern dark:bg-pattern1 h-screen w-full bg-cover pt-28 transition-all duration-300">
      <div className="pl-5 m-auto w-full h-full flex flex-col justify-start items-start lg:max-w-6xl">
        <div className="my-auto">
          <motion.h1
            variants={fadeIn("down", 0.3)}
            initial="hidden"
            whileInView={"show"}
            viewport={{ once: false, amount: 0.6 }}
            className="text-4xl text-white font-ubuntu font-bold mb-5 lg:text-5xl lg:mb-7"
          >
            Ecole Normale <br />
            Supérieure d&apos;Abidjan(ENS)
          </motion.h1>
          <motion.h3
            variants={fadeIn("down", 0.4)}
            initial="hidden"
            whileInView={"show"}
            viewport={{ once: false, amount: 0.6 }}
            className="text-2xl text-white font-ubuntu mb-5 lg:text-3xl lg:mb-7"
          >
            au coeur de votre formation
          </motion.h3>

          <motion.div
            variants={fadeIn("down", 0.6)}
            initial="hidden"
            whileInView={"show"}
            viewport={{ once: false, amount: 0.6 }}
          >
            <Button variant="green" className="group/btn transition">
              Admin
              <div className=" opacity-0 group-hover/btn:opacity-100 group-hover/btn:translate-x-3 transition">
                <ArrowRight />
              </div>
            </Button>
          </motion.div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
