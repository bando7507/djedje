import { Facebook, Instagram, Linkedin, Twitter } from "lucide-react";
import Link from "next/link";
import React from "react";

const Footer = () => {
  return (
    <footer className="pb-4">
      <div className="m-auto w-full h-full px-5 mt-12 lg:max-w-6xl md:flex justify-between">
        <div className="mb-14">
          <Link href="/">
            <h1 className="font-poppins">ENS</h1>
          </Link>
        </div>

        <div className="grid gap-8 md:grid-cols-3">
          <nav>
            <h1 className="mb-4 font-poppins font-bold">A Propos</h1>
            <ul className="flex flex-col gap-2">
              <li>
                <Link href="/">Qui sommes-nous</Link>
              </li>
              <li>
                <Link href="/">Nous rejoindre</Link>
              </li>
              <li>
                <Link href="/">Partenaire</Link>
              </li>
            </ul>
          </nav>
          <nav>
            <h1 className="mb-4 font-poppins font-bold">Produit</h1>
            <ul className="flex flex-col gap-2">
              <li>
                <Link href="/">Fonctionnalités</Link>
              </li>
              <li>
                <Link href="/">FAQ</Link>
              </li>
              <li>
                <Link href="/">Sécurité</Link>
              </li>
            </ul>
          </nav>
          <nav>
            <h1 className="mb-4 font-poppins font-bold">Ressources</h1>
            <ul className="flex flex-col gap-2">
              <li>
                <Link href="/">Blog</Link>
              </li>
              <li>
                <Link href="/">Aide</Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <span className="mt-2 block w-full h-[.5px] bg-muted-foreground" />

      <div className="p-4 flex items-center justify-between">
        <small className="text-muted-foreground tex">
          Copyright © 2023 - 2024 - Ecole Normale Supérieure d&apos;Abidjan
          (Côte d&apos;Ivoire)
        </small>
        <ul className="flex items-center gap-2">
          <li>
            <Facebook color="#d9a13b" />
          </li>
          <li>
            <Linkedin color="#d9a13b" />
          </li>
          <li>
            <Instagram color="#d9a13b" />
          </li>
          <li>
            <Twitter color="#d9a13b" />
          </li>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;
