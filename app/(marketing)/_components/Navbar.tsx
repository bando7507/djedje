"use client";

import React, { ElementRef, useRef, useState } from "react";
import Link from "next/link";
import { cn } from "@/lib/utils";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import Image from "next/image";
import {
  AlignJustify,
  Bell,
  ChevronDown,
  ChevronLeft,
  CopyIcon,
  MoonIcon,
  SunIcon,
} from "lucide-react";
import { Button } from "@/components/ui/button";
import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
} from "@/components/ui/navigation-menu";

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";

import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";

import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { toast } from "sonner";
import { offreSlide } from "@/constants";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

import { useTheme } from "next-themes";

const formSchemaUser = z.object({
  username: z.string().min(2, {
    message: "Username doit contenir au moins 2 caracteres.",
  }),
  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const formSchemaUserCreate = z.object({
  nameuser: z.string().min(3, {
    message: "le nom doit contenir au moins 3 caracteres.",
  }),
  lastname: z.string().min(3, {
    message: "le prenoms doit contenir au moins 3 caracteres.",
  }),
  email: z.string(),
  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
  passwordconfirm: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const formSchemaRec = z.object({
  username: z.string().min(2, {
    message: "Username doit contenir au moins 2 caracteres.",
  }),
  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const formSchemaRecCreate = z.object({
  nameuser: z.string().min(3, {
    message: "le nom doit contenir au moins 3 caracteres.",
  }),
  lastname: z.string().min(3, {
    message: "le prenoms doit contenir au moins 3 caracteres.",
  }),
  email: z.string(),

  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
  passwordconfirm: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const Navbar = () => {
  const [isrecruiter, setIsrecruiter] = useState<boolean>(false);
  const [isrecruiter2, setIsrecruiter2] = useState<boolean>(false);
  const sidebarRef = useRef<ElementRef<"aside">>(null);
  const { setTheme } = useTheme();
  const components: { title: string; href: string; description: string }[] = [
    {
      title: "Alert Dialog",
      href: "/docs/primitives/alert-dialog",
      description:
        "A modal dialog that interrupts the user with important content and expects a response.",
    },
    {
      title: "Hover Card",
      href: "/docs/primitives/hover-card",
      description:
        "For sighted users to preview content available behind a link.",
    },
    {
      title: "Progress",
      href: "/docs/primitives/progress",
      description:
        "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar.",
    },
    {
      title: "Scroll-area",
      href: "/docs/primitives/scroll-area",
      description: "Visually or semantically separates content.",
    },
    {
      title: "Tabs",
      href: "/docs/primitives/tabs",
      description:
        "A set of layered sections of content—known as tab panels—that are displayed one at a time.",
    },
    {
      title: "Tooltip",
      href: "/docs/primitives/tooltip",
      description:
        "A popup that displays information related to an element when the element receives keyboard focus or the mouse hovers over it.",
    },
  ];

  const collapse = () => {
    if (sidebarRef.current) {
      sidebarRef.current.style.left = "0";
    }
  };

  const resetWidth = () => {
    if (sidebarRef.current) {
      sidebarRef.current.style.left = "-100%";
    }
  };

  return (
    <div className="fixed z-[999] left-0 top-0 w-full">
      <div className="w-full flex items-center p-4 justify-between bg-[#fff] dark:bg-[#0A0A0A] dark:text-white lg:py-4 lg:px-0">
        <aside
          ref={sidebarRef}
          className="group/sidebar fixed z-50 top-0 -left-96 w-60 h-full bg-white dark:bg-[#0A0A0A] shadow-xl transition-all ease-in-out duration-300 "
        >
          <div className="relative w-full h-full">
            <div
              onClick={resetWidth}
              className="absolute top-3 right-2 opacity-1 group-hover/sidebar:opacity-100 rounded-sm group-hover/sidebar:bg-neutral-300  "
            >
              <ChevronLeft />
            </div>
            <div className="p-4">
              <div>
                <Link href="/">
                  <h1 className="font-poppins">ENS</h1>
                </Link>
              </div>
              <nav className="mt-7">
                <ul className="font-poppins">
                  <li>
                    <Accordion type="single" collapsible>
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          FORMATIOM
                          <ChevronDown className="h-4 w-4 shrink-0 transition-transform duration-200" />
                        </AccordionTrigger>

                        <AccordionContent>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]/40 text-sm text-muted-foreground"
                            onClick={resetWidth}
                          >
                            Bibliothèque
                          </Link>
                        </AccordionContent>
                        <AccordionContent>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]/40 text-sm text-muted-foreground"
                            onClick={resetWidth}
                          >
                            E-leaning
                          </Link>
                        </AccordionContent>
                        <AccordionContent>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]/40 text-sm text-muted-foreground"
                            onClick={resetWidth}
                          >
                            Jeux
                          </Link>
                        </AccordionContent>
                        <AccordionContent>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]/40 text-sm text-muted-foreground"
                            onClick={resetWidth}
                          >
                            Examen et Concours
                          </Link>
                        </AccordionContent>
                        <AccordionContent>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]/40 text-sm text-muted-foreground"
                            onClick={resetWidth}
                          >
                            Exercice et Corrigés
                          </Link>
                        </AccordionContent>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            Administration
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            Etude
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            Admissions
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            Actualite
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            contact
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                  <li>
                    <Accordion type="single">
                      <AccordionItem value="item-1">
                        <AccordionTrigger>
                          <Link
                            href="/"
                            className="capitalize transition-all duration-300 hover:text-[#1dd1a1]"
                            onClick={resetWidth}
                          >
                            admin
                          </Link>
                        </AccordionTrigger>
                      </AccordionItem>
                    </Accordion>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </aside>
        <div className="lg:m-auto ">
          <Link href="/">
            {/* <h1 className="font-poppins">ENA</h1> */}
            <Image
              src="/logo.png"
              width={50}
              height={100}
              alt=""
              className="object-contain w-full"
            />
          </Link>
        </div>

        <div className="hidden lg:flex gap-9 items-center lg:m-auto">
          <nav className="">
            <ul className="flex gap-6 items-center font-poppins">
              <li>
                <NavigationMenu>
                  <NavigationMenuList>
                    <NavigationMenuItem>
                      <NavigationMenuTrigger className="text-black">
                        FORMATION
                      </NavigationMenuTrigger>
                      <NavigationMenuContent className="flex gap-2 px-7">
                        <div className="w-[50%]">
                          {/* <h1 className="p-6 font-poppins font-semibold text-lg text-[#d9a13b]">
                            Le logiciel
                          </h1> */}
                          <ul>
                            <li className="row-span-3 mt-6">
                              <NavigationMenuLink asChild>
                                <div>
                                  <a
                                    className="flex h-full w-full select-none flex-col justify-end rounded-md bg-gradient-to-b from-muted/50 to-muted p-4 no-underline outline-none focus:shadow-md"
                                    href="/"
                                  >
                                    {/* <Icons.logo className="h-6 w-6" /> */}
                                    <div className="text-lg font-medium mb-4">
                                      ENS
                                    </div>
                                    <p className="text-sm leading-tight text-muted-foreground">
                                      Ecole Normale Supérieure d&apos;Abidjan
                                    </p>
                                  </a>
                                </div>
                              </NavigationMenuLink>
                            </li>
                          </ul>
                        </div>
                        <div className="w-[50%]">
                          <h1 className="p-6 font-poppins font-semibold text-lg text-green-600">
                            Les fonctionnalités en détails
                          </h1>
                          <ul className="grid w-[400px] gap-3 p-4 md:w-[500px] md:grid-cols-3 lg:w-[600px] ">
                            <li>
                              <ListItem
                                href="/"
                                title="Bibliotèque"
                                className="text-muted-foreground capitalize font-poppins text-base"
                              />

                              <ListItem
                                href="/"
                                title="E-learning"
                                className="text-muted-foreground font-poppins text-base"
                              />
                              <ListItem
                                href="/"
                                title="Jeux"
                                className="text-muted-foreground font-poppins text-base"
                              />
                              <ListItem
                                href="/vivier"
                                title="Examen et Concours"
                                className="text-muted-foreground font-poppins text-base"
                              />
                              <ListItem
                                href="/sourcing"
                                title="Exercice et Corrige"
                                className="text-muted-foreground font-poppins text-base"
                              />
                            </li>
                          </ul>
                        </div>
                      </NavigationMenuContent>
                    </NavigationMenuItem>
                  </NavigationMenuList>
                </NavigationMenu>
              </li>
              <li>
                <Link
                  href="/"
                  className="uppercase transition-all text-black duration-300 hover:text-[#1dd1a1]"
                >
                  Administration
                </Link>
              </li>
              <li>
                <Link
                  href="/"
                  className="uppercase transition-all text-black duration-300 hover:text-[#1dd1a1]"
                >
                  Etude
                </Link>
              </li>
              <li>
                <Link
                  href="/"
                  className="uppercase transition-all text-black duration-300 hover:text-[#1dd1a1]"
                >
                  Admissions
                </Link>
              </li>
              <li>
                <Link
                  href="/"
                  className="uppercase transition-all text-black duration-300 hover:text-[#1dd1a1]"
                >
                  Actualite
                </Link>
              </li>
              <li>
                <Link
                  href="/"
                  className="uppercase transition-all text-black duration-300 hover:text-[#1dd1a1]"
                >
                  contact
                </Link>
              </li>
              <li className="bg-[#1dd1a1] text-black h-9 rounded-md px-3 flex justify-center items-center ">
                <Link
                  href="/"
                  className=" block uppercase transition-all duration-300 hover:text-[#1dd1a1]"
                >
                  admin
                </Link>
              </li>
            </ul>
          </nav>

          <div className="flex gap-3 items-center"></div>
        </div>
        <div className="flex items-center gap-5">
          <div onClick={collapse} className="lg:hidden block cursor-pointer">
            <AlignJustify />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;

const ListItem = React.forwardRef<
  React.ElementRef<"a">,
  React.ComponentPropsWithoutRef<"a">
>(({ className, title, children, ...props }, ref) => {
  return (
    <li>
      <NavigationMenuLink asChild>
        <a
          ref={ref}
          className={cn(
            "block select-none space-y-1 rounded-md p-3 leading-none no-underline outline-none transition-colors hover:bg-[#1dd1a1]/40 hover:text-accent-foreground focus:bg-accent focus:text-accent-foreground",
            className
          )}
          {...props}
        >
          <div className="text-sm font-medium leading-none">{title}</div>
          <p className="line-clamp-2 text-sm leading-snug text-muted-foreground">
            {children}
          </p>
        </a>
      </NavigationMenuLink>
    </li>
  );
});
ListItem.displayName = "ListItem";
