"use client"
import React from 'react'
import { CountUp } from 'use-count-up'


type AppProps = {
   end: number,
   duration: number
};

const Counter = ({end, duration}:AppProps) => {
  return (
    <>
        <CountUp isCounting end={end} duration={duration} />
    </>
  )
}

export default Counter