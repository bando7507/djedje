import Navbar from "./_components/Navbar"

const MarketingRootLayout = ({
    children
}: {
    children: React.ReactNode
}) => {

    return(
        <div className="h-full m-auto">
            <Navbar />
            <main className="h-full">
                {children}
            </main>
        </div>
    )
}

export default MarketingRootLayout