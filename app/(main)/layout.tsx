"use client"


import Navbar from "../(marketing)/_components/Navbar";

const MainLayout = ({
    children
}:{
    children: React.ReactNode
}) => {
    return(
        <div className="h-full m-auto">
            <Navbar />
            <main className="h-full pt-28">
                {children}
            </main>
        </div>
    )
}


export default MainLayout;