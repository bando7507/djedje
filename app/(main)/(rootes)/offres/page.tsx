"use client";
import Link from "next/link";
import React, { useState } from "react";

// import { CaretSortIcon, CheckIcon } from "@radix-ui/react-icons"

import { cn } from "@/lib/utils";
import { Button } from "@/components/ui/button";
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
} from "@/components/ui/command";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";

import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormMessage,
  FormLabel,
} from "@/components/ui/form";

import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Briefcase,
  Check,
  ChevronDown,
  MapPin,
  RotateCcw,
  Search,
  Wallet2,
} from "lucide-react";
import { cardOffre } from "@/constants";
import { Checkbox } from "@/components/ui/checkbox";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import Footer from "@/app/(marketing)/_components/Footer";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogTitle,
  DialogTrigger,
} from "@radix-ui/react-dialog";
import { DialogHeader } from "@/components/ui/dialog";
import { toast } from "sonner";

const formSchema = z.object({
  username: z.string().min(2, {
    message: "Username doit contenir au moins 2 caracteres.",
  }),
  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const formSchemarecruiter = z.object({
  nameuser: z.string().min(3, {
    message: "le nom doit contenir au moins 3 caracteres.",
  }),
  lastname: z.string().min(3, {
    message: "le prenoms doit contenir au moins 3 caracteres.",
  }),
  email: z.string(),
  password: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
  passwordconfirm: z.string().min(8, {
    message: "le mot de passe doit contenir au moins 8 caracteres.",
  }),
});

const itemsContrats = [
  {
    id: "cdi",
    label: "CDI",
  },
  {
    id: "cdd",
    label: "CDD",
  },
  {
    id: "freelace",
    label: "Freelance",
  },
  {
    id: "stage",
    label: "Stage",
  },
] as const;

const FormSchemaFiltercontrat = z.object({
  items: z.array(z.string()).refine((value) => value.some((item) => item), {
    message: "Vous devez sélectionner au moins un élément.",
  }),
});

const OffrePage = () => {
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState("");
  // const [checked, setChecked] = React.useState(false)
  const [typeC, setTypeC] = React.useState<string[]>([]);
  const [isrecruiter, setIsrecruiter] = useState<boolean>(false);
  const [nameFiltreJob, setnameFiltreJob] = useState<string>("");

  // const CheckboxReactHookFormMultiple = () => {

  const formFiltercontrat = useForm<z.infer<typeof FormSchemaFiltercontrat>>({
    resolver: zodResolver(FormSchemaFiltercontrat),
    defaultValues: {
      items: [],
    },
  });
  // }

  const onSubmitFiltercontrat = (
    value: z.infer<typeof FormSchemaFiltercontrat>
  ) => {
    console.log(value);
  };

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const formrecruiter = useForm<z.infer<typeof formSchemarecruiter>>({
    resolver: zodResolver(formSchemarecruiter),
    defaultValues: {
      nameuser: "",
      lastname: "",
      email: "",
      password: "",
      passwordconfirm: "",
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do
    // ✅ This will be type-safe and validated.
    console.log(values);
    toast.success("Conncetion réussir");
  }

  function onSubmitFormrecruiter(value: z.infer<typeof formSchemarecruiter>) {
    console.log(value);
    toast.success("Nouvel user créer");
  }

  return (
    <section className="pt-10 px-5 md:px-11 w-full relative dark:text-white">
      <div className="flex items-center gap-2 mt-3 mb-3">
        <Link href="/">Acceuil</Link>
        <span className="block h-4 w-[2px] bg-[#1dd1a1] -rotate-12"></span>
        {nameFiltreJob}
      </div>
      <div className="flex flex-col gap-3 md:flex-row">
        <div>
          <Popover open={open} onOpenChange={setOpen}>
            <PopoverTrigger asChild>
              <Button
                variant="outline"
                role="combobox"
                aria-expanded={open}
                // onChange={value && setnameFiltreJob('')}
                className="w-full md:w-[400px] justify-between"
              >
                {value
                  ? cardOffre.find(
                      (framework) => framework.title.toLowerCase() == value
                    )?.title
                  : "Select framework..."}
                <Search />
              </Button>
            </PopoverTrigger>
            <PopoverContent className=" w-full md:w-[400px] p-0">
              <Command>
                <CommandInput
                  placeholder="Search framework..."
                  className="h-9"
                />
                <CommandEmpty>No framework found.</CommandEmpty>
                <CommandGroup>
                  {cardOffre.map((framework) => (
                    <CommandItem
                      key={framework.id}
                      value={framework.title}
                      onSelect={(currentValue) => {
                        setValue(currentValue == value ? "" : currentValue);
                        setOpen(false);
                        setnameFiltreJob(framework.title);
                        // value == framework.title.toLowerCase()  ? setnameFiltreJob(framework.title) : setnameFiltreJob('')
                      }}
                    >
                      {framework.title}
                      <Check
                        size={20}
                        className={`ml-auto ${
                          value == framework.title.toLowerCase()
                            ? "opacity-95"
                            : "opacity-0"
                        }`}
                      />
                    </CommandItem>
                  ))}
                </CommandGroup>
              </Command>
            </PopoverContent>
          </Popover>
        </div>
        <div>
          <Popover>
            <PopoverTrigger asChild>
              <Button variant="outline">
                Contrat <ChevronDown />
              </Button>
            </PopoverTrigger>
            <PopoverContent className="w-80">
              <Form {...formFiltercontrat}>
                <form
                  onSubmit={formFiltercontrat.handleSubmit(
                    onSubmitFiltercontrat
                  )}
                  className="space-y-3"
                >
                  <FormField
                    control={formFiltercontrat.control}
                    name="items"
                    render={() => (
                      <FormItem>
                        <div className="mb-1">
                          {/* <FormLabel className="text-base">Sidebar</FormLabel>
                          <FormDescription>
                            Select the items you want to display in the sidebar.
                          </FormDescription> */}
                        </div>
                        {itemsContrats.map((item) => (
                          <FormField
                            key={item.id}
                            control={formFiltercontrat.control}
                            name="items"
                            render={({ field }) => {
                              return (
                                <FormItem
                                  key={item.id}
                                  className="flex flex-row items-start space-x-3 space-y-0"
                                >
                                  <FormControl>
                                    <Checkbox
                                      // onChange={() => setTypeC(item.id)}
                                      checked={field.value?.includes(item.id)}
                                      onCheckedChange={(checked) => {
                                        return checked
                                          ? field.onChange(
                                              [...field.value, item.id],
                                              setTypeC((el) => [
                                                ...el,
                                                item.label,
                                              ])
                                            )
                                          : field.onChange(
                                              field.value?.filter(
                                                (value) => value !== item.id
                                              ),
                                              setTypeC(
                                                typeC.filter(
                                                  (value) =>
                                                    value !== item.label
                                                )
                                              )
                                            );
                                      }}
                                    />
                                  </FormControl>
                                  <FormLabel className="text-sm font-normal">
                                    {item.label}
                                  </FormLabel>
                                </FormItem>
                              );
                            }}
                          />
                        ))}
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <hr />
                  <div className="flex justify-end gap-7 items-center">
                    <div className="text-center">
                      {
                        cardOffre.filter((item) =>
                          new Set(typeC).has(item.type)
                        ).length
                      }
                      {/* {console.log(typeC)} */}
                      <p className="text-[12px]">offres</p>
                    </div>
                    {/* <div>
                      <Button variant='green' type="submit">Appliquer</Button>
                    </div> */}
                  </div>
                  {/* <Button type="submit" variant='green'>Submit</Button> */}
                </form>
              </Form>
            </PopoverContent>
          </Popover>
        </div>

        {nameFiltreJob != "" && (
          <Button
            onClick={() => {
              setValue("");
              setnameFiltreJob("");
            }}
            variant="ghost"
          >
            Réinitialiser
            <RotateCcw size={24} className="ml-3" />
          </Button>
        )}
      </div>
      <div>
        <h1>Votre recherche renvoie {cardOffre.length} résultats</h1>
      </div>

      <div>
        <div className="relative w-full flex flex-col pt-10  gap-3 lg:flex-row ">
          <div className="flex-1">
            {value != "" || typeC.length != 0
              ? cardOffre
                  .filter(
                    (el) =>
                      el.title == nameFiltreJob || new Set(typeC).has(el.type)
                  )
                  .map((item) => (
                    <Link href={`/offres/${item.id}`} key={item.id}>
                      <div className="flex flex-col gap-4 bg-red-200 shadow-xl rounded-lg p-5  mb-5 overflow-hidden md:flex-row dark:bg-[#28282B] ">
                        <div className="w-[60%]">
                          <span
                            className={`inline-block ${
                              item.type == "CDI"
                                ? "bg-orange-300"
                                : "bg-[#1dd1a1]"
                            } p-1 rounded-full text-sm text-white font-poppins mt-2`}
                          >
                            {item.type}
                          </span>
                          <div>
                            <div className="mb-6">
                              {/* <span className='inline-block text-base italic text-black font-poppins'>Offre d'emploi</span> */}
                              <h1 className="font-ubuntu font-bold text-black text-lg md:text-xl lg:text-2xl mb-2 dark:text-white">
                                {item.title} ssssss
                              </h1>
                              <h2 className="font-ubuntu text-black text-md md:text-lg dark:text-white">
                                {item.name}
                              </h2>
                              <p>
                                {item.description.length > 300
                                  ? item.description.slice(0, 300) + "..."
                                  : item.description}
                              </p>
                            </div>
                            <h2 className="text-base text-[#1dd1a1] font-poppins font-semibold">
                              Voir cette offre
                            </h2>
                          </div>
                        </div>
                        <div className="bg-[#dddddd1e] rounded-lg flex-1">
                          <div className="flex flex-col gap-2  p-4">
                            <div>
                              <small>TJM</small>
                              <div className="flex items-center gap-1">
                                <Wallet2 />
                                <p>{item.wallet}</p>
                              </div>
                            </div>
                            <div>
                              <Briefcase />
                            </div>
                            <div>
                              <small>Lieu</small>
                              <div className="flex items-center gap-1">
                                <MapPin />
                                <p>{item.localisation}</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Link>
                  ))
              : cardOffre.map((item) => (
                  <Link href={`/offres/${item.id}`} key={item.id}>
                    <div className="flex flex-col gap-4 bg-white shadow-xl rounded-lg p-5  mb-5 overflow-hidden md:flex-row dark:bg-[#28282B] ">
                      <div className="w-[60%]">
                        <span
                          className={`inline-block ${
                            item.type == "CDI"
                              ? "bg-orange-300"
                              : "bg-[#1dd1a1]"
                          } p-1 rounded-full text-sm text-white font-poppins mt-2`}
                        >
                          {item.type}
                        </span>
                        <div>
                          <div className="mb-6">
                            {/* <span className='inline-block text-base italic text-black font-poppins'>Offre d'emploi</span> */}
                            <h1 className="font-ubuntu font-bold  text-black text-lg md:text-xl lg:text-2xl mb-2 dark:text-white">
                              {item.title}
                            </h1>
                            <h2 className="font-ubuntu text-black text-md md:text-lg dark:text-white">
                              {item.name}
                            </h2>
                            <p>
                              {item.description.length > 300
                                ? item.description.slice(0, 300) + "..."
                                : item.description}
                            </p>
                          </div>
                          <h2 className="text-base text-[#1dd1a1] font-poppins font-semibold">
                            Voir cette offre
                          </h2>
                        </div>
                      </div>
                      <div className="bg-[#dddddd1e] rounded-lg flex-1">
                        <div className="flex flex-col gap-2  p-4">
                          <div>
                            <small>TJM</small>
                            <div className="flex items-center gap-1">
                              <Wallet2 />
                              <p>{item.wallet}</p>
                            </div>
                          </div>
                          <div>
                            <Briefcase />
                          </div>
                          <div>
                            <small>Lieu</small>
                            <div className="flex items-center gap-1">
                              <MapPin />
                              <p>{item.localisation}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                ))}
          </div>
          <div className="">
            <div className="bg-white shadow-md rounded-xl w-full lg:w-[400px] p-4 dark:bg-[#28282B]">
              <h2 className="font-ubuntu font-bold text-black text-center text-lg md:text-xl dark:text-white">
                Déposez votre CV
              </h2>

              <ul className="flex flex-col gap-5 mt-6 mb-4">
                <li>
                  <h2>• Fixez vos conditions</h2>
                  Rémunération, télétravail... Définissez tous les critères
                  importants pour vous.
                </li>
                <li>
                  <h2>• Faites-vous chasser</h2>
                  Les recruteurs viennent directement chercher leurs futurs
                  talents dans notre CVthèque.
                </li>
                <li>
                  <h2>• 100% gratuit</h2>
                  Aucune commission prélevée sur votre mission freelance.
                </li>
              </ul>
              {/* <Button variant='green'>Creer mon profil</Button> */}
              <Dialog>
                <DialogTrigger className="bg-[#1dd1a1] text-white h-9 rounded-md px-3">
                  Creer mon profil
                </DialogTrigger>
                <DialogContent>
                  <DialogHeader>
                    <DialogTitle asChild>
                      <h1 className="text-center">YLSIX RECRUITERS</h1>
                    </DialogTitle>
                    <DialogDescription>
                      This action cannot be undone. This will permanently delete
                      your account and remove your data from our servers.
                    </DialogDescription>
                  </DialogHeader>
                  {!isrecruiter && (
                    <Form {...form}>
                      <form
                        onSubmit={form.handleSubmit(onSubmit)}
                        className="space-y-5"
                      >
                        <FormField
                          control={form.control}
                          name="username"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Username/Email</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Username/Email"
                                  type="email"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <FormField
                          control={form.control}
                          name="password"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Password</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Password"
                                  type="password"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <Button type="submit" variant="green">
                          Se connecter
                        </Button>
                      </form>
                    </Form>
                  )}
                  {isrecruiter && (
                    <Form {...formrecruiter}>
                      <form
                        onSubmit={formrecruiter.handleSubmit(
                          onSubmitFormrecruiter
                        )}
                        className="space-y-5"
                      >
                        <FormField
                          control={formrecruiter.control}
                          name="nameuser"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Nom</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Nom"
                                  type="text"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <FormField
                          control={formrecruiter.control}
                          name="lastname"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Prenoms</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Prenoms"
                                  type="text"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <FormField
                          control={formrecruiter.control}
                          name="email"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Email</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="email"
                                  type="email"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <FormField
                          control={formrecruiter.control}
                          name="password"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Mot de passe</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Mot de passe"
                                  type="password"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <FormField
                          control={formrecruiter.control}
                          name="passwordconfirm"
                          render={({ field }) => (
                            <FormItem>
                              <FormLabel>Confirmer mot de passe</FormLabel>
                              <FormControl>
                                <Input
                                  placeholder="Mot de passe"
                                  type="password"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </FormItem>
                          )}
                        />
                        <Button type="submit" variant="green">
                          Se connecters
                        </Button>
                      </form>
                    </Form>
                  )}
                  <div className="flex justify-end gap-1">
                    {!isrecruiter && (
                      <>
                        <h1>Vous n&apos;avez pas de compte?</h1>
                        <span
                          onClick={() => setIsrecruiter(true)}
                          className="cursor-pointer transition hover:text-[#1dd1a1]"
                        >
                          S&apos;inscrire
                        </span>
                      </>
                    )}
                    {isrecruiter && (
                      <>
                        <h1>Vous avez déja un compte?</h1>
                        <span
                          onClick={() => setIsrecruiter(false)}
                          className="cursor-pointer transition hover:text-[#1dd1a1]"
                        >
                          Se connecter
                        </span>
                      </>
                    )}
                  </div>
                </DialogContent>
              </Dialog>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </section>
  );
};

export default OffrePage;
